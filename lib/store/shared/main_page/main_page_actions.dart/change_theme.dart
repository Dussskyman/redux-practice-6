import 'package:flutter/material.dart';

class ChangeTheme {
  final ThemeData themeData;

  ChangeTheme(this.themeData);
}
