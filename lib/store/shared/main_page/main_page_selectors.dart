import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice_6/store/application/app_state.dart';
import 'package:redux_practice_6/store/shared/main_page/main_page_actions.dart/change_locale.dart';
import 'package:redux_practice_6/store/shared/main_page/main_page_actions.dart/change_theme.dart';

class MainPageSelector {
  static void Function(String) changeLanguageAction(Store<AppState> store) {
    return (String langCode) => store.dispatch(ChangeLocale(langCode));
  }

  static String getLanguage(Store<AppState> store) {
    return store.state.mainPageState.language;
  }

  static void Function(ThemeData themeData) changeThemeAction(
      Store<AppState> store) {
    return (themeData) => store.dispatch(ChangeTheme(themeData));
  }

  static ThemeData getTheme(Store<AppState> store) {
    return store.state.mainPageState.themeData;
  }
}
