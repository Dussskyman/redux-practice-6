import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux_practice_6/dictionary/flutter_dictionary.dart';
import 'package:redux_practice_6/res/locales.dart';
import 'package:redux_practice_6/store/reducer.dart';
import 'package:redux_practice_6/store/shared/main_page/main_page_actions.dart/change_locale.dart';
import 'package:redux_practice_6/store/shared/main_page/main_page_actions.dart/change_theme.dart';

class MainPageState {
  String language;
  ThemeData themeData;

  MainPageState({
    @required this.language,
    @required this.themeData,
  });

  factory MainPageState.initial() {
    return MainPageState(
        language: Locales.en, themeData: ThemeData(primaryColor: Colors.blue));
  }

  MainPageState copyWith({
    String language,
    ThemeData themeData,
  }) {
    return MainPageState(
      language: language ?? this.language,
      themeData: themeData ?? this.themeData,
    );
  }

  MainPageState reducer(dynamic action) {
    return Reducer<MainPageState>(
        actions: HashMap.from({
      ChangeLocale: (action) => _languageChoose(action),
      ChangeTheme: (action) => _themeChoose(action),
    })).updateState(action, this);
  }

  MainPageState _languageChoose(ChangeLocale action) {
    FlutterDictionary.instance.setNewLanguage(action.languageCode);

    return copyWith(language: action.languageCode);
  }

  MainPageState _themeChoose(ChangeTheme action) {
    return copyWith(themeData: action.themeData);
  }
}
