import 'package:flutter/material.dart';
import 'package:redux_practice_6/store/shared/main_page/main_page_state.dart';

class AppState {
  final MainPageState mainPageState;
  AppState({
    @required this.mainPageState,
  });

  factory AppState.initial() {
    return AppState(mainPageState: MainPageState.initial());
  }

  static AppState getReducer(AppState state, dynamic action) {
    return AppState(mainPageState: state.mainPageState.reducer(action));
  }
}
