import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice_6/store/application/app_state.dart';
import 'package:redux_practice_6/store/shared/main_page/main_page_selectors.dart';

class MainPageViewModel {
  final String language;
  final ThemeData themeData;
  final void Function(String language) changeLanguage;
  final void Function(ThemeData themeData) changeTheme;

  MainPageViewModel({
    @required this.language,
    @required this.themeData,
    @required this.changeLanguage,
    @required this.changeTheme,
  });

  static MainPageViewModel fromStore(Store<AppState> store) {
    return MainPageViewModel(
      language: MainPageSelector.getLanguage(store),
      themeData: MainPageSelector.getTheme(store),
      changeLanguage: MainPageSelector.changeLanguageAction(store),
      changeTheme: MainPageSelector.changeThemeAction(store),
    );
  }
}
