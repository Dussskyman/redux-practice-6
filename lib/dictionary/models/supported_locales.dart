import 'package:flutter/material.dart';
import 'package:redux_practice_6/dictionary/data/en.dart';
import 'package:redux_practice_6/dictionary/data/ru.dart';

import 'package:redux_practice_6/dictionary/models/supported_language.dart';
import 'package:redux_practice_6/res/locales.dart';

class SupportedLocales {
  List<SupportedLanguage> _supportedLocales;

  SupportedLocales._() {
    _supportedLocales = <SupportedLanguage>[
      SupportedLanguage(
        languageCode: Locales.en,
        language: en,
      )..choose(),
      SupportedLanguage(
        languageCode: Locales.ru,
        language: ru,
      ),
    ];
  }

  static SupportedLocales instance = SupportedLocales._();

  void changeLocale(String languageCode) {
    _supportedLocales
        .firstWhere((SupportedLanguage supLang) => supLang.isSelected)
        ?.discard();
    _supportedLocales
        .firstWhere(
            (SupportedLanguage supLang) => supLang.languageCode == languageCode)
        ?.choose();
  }

  List<Locale> get getSupportedLocales {
    return _supportedLocales
            ?.map((SupportedLanguage supLang) => supLang.getLocale)
            ?.toList() ??
        <SupportedLanguage>[];
  }

  String get getCurrentLocale {
    return _supportedLocales
            ?.firstWhere((SupportedLanguage supLang) => supLang.isSelected)
            ?.languageCode ??
        Locales.base;
  }

  SupportedLanguage getSupportedLanguage(Locale locale) {
    return _supportedLocales.firstWhere((SupportedLanguage supLang) =>
        supLang.languageCode == locale.languageCode);
  }
}
