import 'package:flutter/material.dart';
import 'package:redux_practice_6/dictionary/dictionary_classes/main_page.dart';

class Language {
  final MainPageLanguage mainPageLanguage;
  const Language({
    @required this.mainPageLanguage,
  });
}
