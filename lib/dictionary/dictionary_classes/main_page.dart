import 'package:flutter/material.dart';

class MainPageLanguage {
  final String title;
  final String subtitle;
  final String thirdtitle;

  const MainPageLanguage({
    @required this.title,
    @required this.subtitle,
    @required this.thirdtitle,
  });
}
