import 'package:redux_practice_6/dictionary/dictionary_classes/main_page.dart';
import 'package:redux_practice_6/dictionary/models/language.dart';

const Language en = Language(
  mainPageLanguage: MainPageLanguage(
    title: 'This is title',
    subtitle: 'This is subtitle',
    thirdtitle: 'This is third title, btw',
  ),
);
