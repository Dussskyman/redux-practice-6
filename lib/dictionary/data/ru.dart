import 'package:redux_practice_6/dictionary/dictionary_classes/main_page.dart';
import 'package:redux_practice_6/dictionary/models/language.dart';

const Language ru = Language(
  mainPageLanguage: MainPageLanguage(
    title: 'Это заголовок',
    subtitle: 'Это под заголовок',
    thirdtitle: 'А это третий заголовок, кста',
  ),
);
