import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice_6/dictionary/dictionary_classes/main_page.dart';
import 'package:redux_practice_6/dictionary/flutter_delegate.dart';
import 'package:redux_practice_6/dictionary/flutter_dictionary.dart';
import 'package:redux_practice_6/dictionary/models/supported_locales.dart';
import 'package:redux_practice_6/main_vm.dart';
import 'package:redux_practice_6/store/application/app_state.dart';

void main() {
  final Store store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store store;

  const MyApp({Key key, this.store}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    FlutterDictionary.instance
        .setNewLanguage(FlutterDictionaryDelegate.getCurrentLocale);
    return StoreProvider<AppState>(
      store: store,
      child: StoreConnector<AppState, AppState>(
          converter: (store) => store.state,
          builder: (context, vm) {
            return MaterialApp(
              theme: vm.mainPageState.themeData,
              locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
              supportedLocales: SupportedLocales.instance.getSupportedLocales,
              localizationsDelegates:
                  FlutterDictionaryDelegate.getLocalizationDelegates,
              home: MyHomePage(),
            );
          }),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int dropVal1 = 0;

  int dropVal2 = 1;

  @override
  Widget build(BuildContext context) {
    final MainPageLanguage mainPageLanguage =
        FlutterDictionary.instance.language.mainPageLanguage;
    return StoreConnector<AppState, MainPageViewModel>(
      converter: (store) => MainPageViewModel.fromStore(store),
      builder: (context, vm) => Scaffold(
        drawer: Drawer(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              DropdownButton(
                value: dropVal1,
                items: [
                  DropdownMenuItem<int>(
                    value: 0,
                    child: InkWell(
                      child: Text('Change theme to blue'),
                    ),
                  ),
                  DropdownMenuItem<int>(
                    value: 1,
                    child: InkWell(
                      child: Text('Change theme to red'),
                    ),
                  ),
                ],
                onChanged: (value) {
                  dropVal1 = value;
                  if (value == 0) {
                    return vm.changeTheme(ThemeData(primaryColor: Colors.blue));
                  }
                  return vm.changeTheme(ThemeData(primaryColor: Colors.red));
                },
              ),
              DropdownButton(
                value: dropVal2,
                items: [
                  DropdownMenuItem<int>(
                    value: 0,
                    child: InkWell(
                      child: Text('Change locale to ru'),
                    ),
                  ),
                  DropdownMenuItem<int>(
                    value: 1,
                    child: InkWell(
                      child: Text('Change locale to en'),
                    ),
                  ),
                ],
                onChanged: (value) {
                  dropVal2 = value;
                  if (value == 0) {
                    return vm.changeLanguage('ru');
                  }
                  return vm.changeLanguage('en');
                },
              ),
            ],
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              color: Theme.of(context).primaryColor,
              width: double.infinity,
              child: Text(
                mainPageLanguage.title,
                style: TextStyle(fontSize: 40),
              ),
            ),
            Container(
              color: Theme.of(context).primaryColor,
              width: double.infinity,
              child: Text(
                mainPageLanguage.subtitle,
                style: TextStyle(fontSize: 24),
              ),
            ),
            Container(
              color: Theme.of(context).primaryColor,
              width: double.infinity,
              child: Text(
                mainPageLanguage.thirdtitle,
                style: TextStyle(fontSize: 20),
              ),
            )
          ],
        ),
      ),
    );
  }
}
